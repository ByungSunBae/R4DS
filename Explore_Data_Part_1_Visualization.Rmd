---
title: "Explore Data Part 1 - Data Visualization"
date: "`r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: kate
---


```{r knitr_init, echo=FALSE, cache=FALSE}
library(knitr)
library(rmdformats)

## Global options
options(max.print="75")
opts_chunk$set(echo=TRUE,
	             cache=FALSE,
               prompt=FALSE,
               tidy=TRUE,
               comment=NA,
               message=FALSE,
               warning=FALSE)
opts_knit$set(width=75)
```

# Introduction

 - 데이터 탐색은 데이터를 살펴보고, 빠르게 가설을 만들고 테스팅하는 작업을 계속 반복하는 것이다.
 
 - 데이터 탐색의 목표는 후에 분석을 더욱 깊게 하도록 이끌어준다.
 
![](http://r4ds.had.co.nz/diagrams/data-science-explore.png)

 - 탐색을 한다는 것은 기본적으로 데이터 변환 + 시각화 과정이 포함되어있다.
 
 - 데이터 시각화( __Data Visualization__ )에서는 `ggplot2` plot의 기본적인 구조를 배우고, 데이터를 plot으로 옮기는 기술을 배울 것이다.
 
 - 데이터 시각화 하나만으로는 충분하지 않기 때문에 데이터 변환( __Data Transform__ )에서 중요한 변수를 선택하는 방법, 관측치( observations )를 filtering하는 방법, 새로운 변수 생성, 그리고 summaries를 계산하는 것을 배울 것이다.
 
 - 위 두가지를 먼저 보고나서 탐색적 자료 분석( __Exploratory Data Analysis__ )을 살펴볼 것이다. 변환 + 시각화를 우리의 궁금증과 회의적인 태도와 같이 이용해서 데이터에 대해 질문하고 답하는 것을 살펴볼 것이다.
 
 - 모델링( __Modeling__ )도 데이터 탐색 과정에서 중요한 부분을 차지한다.
 
 - 모델링은 데이터를 다루는 방법 + 프로그래밍에 더 익숙해지면 보는 걸 추천한다.
    * 이미 익숙한 분들은 따로 보셔도 상관없습니다..

 - 본 문서는 데이터 탐색과정에 필요한 데이터 시각화 내용을 정리하였다.
 
 - R4DS 교재에 있는 Exercises는 따로 다룰 예정이다.
 
<br>

# Data Visualization (ggplot2)

 - 위에서 얘기했던 것처럼 `ggplot2`를 이용하여 우리의 데이터를 시각화하는 방법을 배워보자.
 
 - R에 내장되어있는 `plot()` 함수를 이용하여 그래프를 그릴 수 있지만, `ggplot2`를 이용하면 훨씬 멋지고, 다용도의 그래프를 시각화할 수 있다.

## Prerequisites

 - `ggplot2`는 그래프를 만들기 위해 일관성있는 시스템인 그래픽 문법( __grammar of graphics__ )을 이용한다.
     * 그래프를 그릴 때 사용하는 문법이 일관성있다는 의미인데, 나중에 코드를 살펴보면 바로 알 수 있다.
     
 - `ggplot2`는 `tidyverse` 패키지내의 핵심적인 구성요소중 하나이다.
 
 - `ggplot2`를 따로 1개만 불러와도 되지만, `tidyverse` 패키지를 불러옴으로써, `dplyr`, `tidyr`등도 한번에 불러올 수 있다.
 
 - 어차피 `dplyr`, `tidyr`를 같이 사용하기 때문에 `tidyverse`를 불러왔다.

```{r}
library(tidyverse)
```

 - 이 `tidyverse`라는 패키지는 메타 패키지로써, __Hadley Wickham__ 이 말하는 분석과정에 필요한 모든 패키지를 포함하고 있다.
 
 - 아직 설치를 하지 않았다면 아래와 같은 코드를 이용해서 `tidyverse`를 설치하면 된다.

```{r, eval=FALSE}
install.packages("tidyverse")
library(tidyverse)
```

 - 패키지는 한번만 설치하면 된다.
 
 - 하지만 R을 다시 시작할때마다 패키지를 다시 불러와야한다.
 
 - 원하는 패키지의 함수만 사용하고 싶다면, `package::function()`과 같은 꼴로 코드를 실행하면 된다.
    * 예를 들어, `ggplot2::ggplot()`는 `ggplot2` 패키지에 있는 `ggplot()` 함수를 사용하겠다는 의미이다.

## First steps

 - 본 문서에서 사용할 데이터는 `mpg`와 `diamonds` 데이터셋이다.
 
 - 먼저 `mpg` 데이터셋을 이용하여 시각화를 해보자.
    * `mpg`는 `ggplot2` 패키지에 포함된 데이터셋이다.
 
 - `mpg`을 확인해보면 다음과 같다.

```{r}
mpg
```

 - `mpg`는 38개의 자동차 모델에 대한 US Environmental Protection Agency에서 수집한 데이터이다.
 
 - 관측치개수 : 234 / 변수개수 : 11
 
 - `mpg`의 변수들중 2가지를 살펴보자.
    * `displ` : 자동차의 엔진사이즈, 단위 : 리터
    * `hwy` : 고속도로에서 자동차의 연료 효율 ( 연비 ), 단위 : 갤런당 마일, 같은 거리를 달릴 때 연비가 낮을수록 연료를 더 많이 소비한다.
 
 - `displ`과 `hwy`를 살펴보았을때, 아래와 같은 질문을 할 수 있다.
    * 자동차의 엔진사이즈가 크면, 엔진사이즈가 작은 자동차에 비해 연료를 더 많이 소모할까?
    * 엔진크기와 연비는 비례관계인가?
    
 - 물론 위 2개의 질문은 R4DS 교재에서 제시한 궁금증이다.
    * 사람마다 다르기 때문에 저마다 궁금한 내용이 다를 수 있고, 그렇기 때문에 다양한 그림들을 그릴 수 가 있다.
    * R4DS 교재에서 제시하는 질문을 토대로 이 문서를 작성하되, 필자가 실제로 궁금한 내용도 같이 넣을 생각이다.
    
 - `mpg`에 대해 더 자세히 알고싶다면, `?mpg`를 실행하여 확인하면된다.

## Creating a ggplot

 - `mpg`를 시각화하기 위해, x축은 `displ`, y축은 `hwy`로 셋팅하여 코드를 실행하였다.

```{r}
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy))
```

 - 그래프 설명
    * 위 그래프는 자동차 엔진 크기( __displ__ )와 연비( __hwy__ )간에 음의 관계를 보여준다.
 
    * 즉, 자동차의 엔진 크기가 클수록 연비가 낮아져 연료를 더 많이 사용한다.
 
    * 위 그래프는 엔진크기와 연비에 대한 가설을 확인하거나 반박하는가?
 
 - 코드 설명
    * `ggplot(data = mpg)` : `ggplot()`함수는 layers를 추가할 수 있는 좌표계를 만든다.
        * 쉽게 말해, 그림을 그리기 전에 스케치북을 준비한다고 이해하면 된다.
        * 그리고 시각화를 하는데 있어 어떤 데이터를 이용할지는 `data` argument를 이용하면된다.
        
    * `geom_point(mapping = aes(x = displ, y = hwy))` : 실제로 scatter plot 등의 시각화자료를 생성하기 위해서는 `geom`계열의 함수를 추가해야 구체적으로 그려진다.
        * `geom`계열의 함수들은 기하학적 객체를 의미하며, 이 부분이 위에서 말한 layers를 의미한다.
        * 그리고 이러한 layer를 여러개 추가하여 다른 type의 그래프를 시각화할수 있다.
        * `geom` 함수들은 `mapping`이라는 argument가 있다. 이는 우리의 데이터셋에서의 변수들과 시각적 특징들을 어떻게 mapping할지를 정의한다.
        * `mapping` argument는 항상 `aes()` 함수가 붙는데, 이 함수의 argument중 `x`와 `y`는 각각 x축과 y축을 어떤 변수로 할 것인지를 구체적으로 지정한다.
        * `aes()` 함수의 argument는 `x`, `y` 이외에도 여러가지 있는데 나중에 배울예정이니 조금만 참아보자..ㅎㅎ
    
## A graphing template

 - 기본적으로 `ggplot2`패키지를 활용한 시각화는 아래와 같은 template을 사용한다.

```{r, eval=FALSE}
ggplot(data = <DATA>) + 
  <GEOM_FUNCTION>(mapping = aes(<MAPPINGs>))
```

 - 본 문서의 나머지는 위 template을 완성시키고 확장하여 어떻게 다른 type의 그래프를 그리는지를 보여줄 것이다.
 
 - `<MAPPINGS>` 부분을 먼저 살펴보자.

## Aesthetic mappings

 - 아래의 plot에서, 빨간색으로 강조된 포인트들은 우리가 확인했던 linear trend로부터 벗어나는 것처럼 보인다.
 
 - 이러한 자동차들은 예상보다 높은 주행거리를 가지고있다. 어떻게 이러한 자동차들을 설명해야할까?

```{r, echo=FALSE}
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy, colour = (displ > 5 & hwy > 20))) + 
  scale_color_manual(values=c("black", "red")) + theme(legend.position = "None")
```

 - 먼저,  **"이 자동차들은 하이브리드 방식이다."** 라고 가설을 세워보자.
 
 - 위 가설을 테스트하는 한가지 방법은 각각의 자동차의 `class` 값을 보는 것이다.
 
 - `mpg` 데이터의 `class` 변수는 자동차들을 소형, 중형 그리고 SUV와 같은 그룹으로 분류한다.
 
 - 만약 빨간색으로 칠해진 포인트들이 하이브리드 방식이라하면, 그것들은 소형차 혹은 초소형차로 분류되어야한다. 
    * 명심해야할 것은 하이브리드 트럭이나 SUV가 대중화되기이전에 데이터가 수집되었다는 점이다.
    * 필자는 처음 읽었을때 조금 당황했다..ㅎㅎ 
    * 하이브리드면 자동차 파워방식이 2개이상(전기+디젤 등)이라 SUV나 트럭급이 되어야한다는 생각을 했기때문이다. (대중화되기전에 데이터를 수집했다고하니, 그려러니하고 넘어갔다..)
    
 - 우리는 시각화를 할때, `class`와 같은 3번째 변수를 미학적 요소( __aesthetic__ )에 mapping하여 2차원의 scatter plot에 정보를 추가할 수 있다.

 - aesthetic는 그래프내 객체들의 시각적 특징을 말한다.
    * 이해가 안될 수 있는데, 결국은 포인트의 크기, 색깔이나 Bar의 색깔 등을 말한다.
    * 이러한 시각적 특징에 다른 변수들의 값을 반영할 수 있다.
    * 다차원의 정보를 여러 시각적 특징에 반영함으로써, 2차원 그래프에 다양한 정보들을 표현할 수 있다.
    
![](http://r4ds.had.co.nz/visualize_files/figure-html/unnamed-chunk-7-1.png)

 - 설명은 여기까지하고 이제 `class` 변수를 scatter plot의 색깔로 반영하여 시각화해보자.

```{r}
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy, color = class))
```

```{r}

```

```{r}

```

```{r}

```

```{r}

```

```{r}

```

```{r}

```

```{r}

```

```{r}

```





